\documentclass[9pt]{extarticle}
\usepackage[margin=0.7in]{geometry}
\usepackage{multicol}               % Columns
\usepackage{tabularx}               % Extra table functionality
\setlength{\skip\footins}{1.0cm}    % More space between text and footnotes
\usepackage{color}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{ {/home/justinting/programming/elec5616_project_3} }
\usepackage[hyperfootnotes=false]{hyperref}
\hypersetup{
    colorlinks,
    citecolor=blue,
    linkcolor=red,
    urlcolor=blue,
    citebordercolor=red,
    filebordercolor=red,
    linkbordercolor=blue
}
\usepackage[round]{natbib}          % Round brackets when citep*-ing sources
\usepackage{comment}                % Comments...
\usepackage{titlesec}
\usepackage[parfill]{parskip}       % Newline instead of indentation per paragraph
\usepackage{enumitem}
\usepackage{textcomp}
\newcommand{\code}[1]{\texttt{#1}}
\setlist{nosep}
\setlength{\columnsep}{0.5cm}       % Separate columns (when used) by 0.5cm
% \setcounter{secnumdepth}{4}         % Give paragraphs 'numbers'

\title{ELEC5616 Assignment 3}
\author{Sammi Chow, Justin Ting}
\date{May 2016}

\begin{document}
\maketitle

\begin{multicols}{2}

\section{Low Level Exploits}
\subsection{Savegame}
\subsubsection{Creating Buffer Overflows}

By default, C aligns structs to 4-byte words, such that there is actually 2 bytes of padding following the char name[10] array, making the Hero struct have a size of 20 bytes, as opposed 18 bytes which is simply a sum of the size of its individual constituents. Such features can be turned off with special compiler flags, but there is no reason to do so when working on computers (may be a different story in embedded systems where every bit counts). 

The minimal case required for a buffer overflow is a 12 character name, which will cause the \lq gold\rq variable to take the value 0 - and any more characters thereafter only causing the gold value to increase more and more (until the overflowed integer grows so large it overrids the return address with most likely a garbage one, causing the program to segmentation fault when trying to exit). Thus, we simply need to enter a sufficiently large amount of characters beyond the 13th to obtain an overflowed gold value of over 9000.

To understand the specific mechanics of the buffer overflow, we first look at the minimal case using 12 characters, where the overflow causes gold to take the value 0. Unless strings in C are assigned to char arrays with defined size, e.g. char foo[25], they are null terminated by \lq0\rq, which is equivalent to the integer 0. This can be confirmed by evaluating sizeof(\lq\textbackslash0\rq), which returns 4 bytes (a character alone is 1 byte, not 4!). As such, the 12 character username's null terminator overflows an entire 4-byte 0-integer into the gold variable.

    It follows that to obtain larger numbers, we simply need to enter more characters to obtain increasingly large numbers. To obtain the first number larger than 9000, i.e. 9001, we use \lq )\#\rq \ - this is represented by the binary number $0010001100101001_2 = 8192_{10} + 512_{10} + 256_{10} + 32_{10} + 8_{10} + 1_{10} = 9001_{10}$. However, if we observe the binary representation of ) and \# seperately, we get $00101001$ and $00100011$ respectively - which are in \lq reverse\rq \ order to what we see in our binary number. This is because the machine used (along with essentially all modern consumer devices such as laptops, computers, etc.) is little endian, meaning that the least significant byte in our word is read first, i.e. the characters are read in reversed order to which they are entered.

\subsubsection{Preventing Buffer Overflows}

    This buffer overflow exploit can be prevented through the use of canaries, where the prevention  can result in program termination (as opposed to allowing continued execution of the program). The canary's purpose is to act as a flag that can be used to check whether overflows have occurred, in which case the canary's value would no longer have its original value, indicating the overflow, at which point the program would exit to prevent any unexpected behaviour as a result of the overflow. While they are a feature built into gcc as a part of -fstack-protector they only perform checks on buffer larger than 8 bytes (further details on canaries can be found in \textbf{Question 1.3.1}), which will not prevent our gold variable from being corrupted. To prevent this, we would have to perform our own bounds checking by placing a custom canary between name and gold, which could be populated with a random variable at compile time that is stored globally. By checking this after reading the user's name, we can confirm whether an overflow occurred by verifying our custom canary, and exiting the program if it has been corrupted.

\subsubsection{Exploiting Buffer Overflows}
    Exploiting buffer overflows could be useful for more than just a game with the -fno-stack-protector flag enabled, by allowing access into a program/system that requires authentication. In the above case, our overflow allowed a hack of the gold value - but in a different code base, the corrupted variable could be a boolean confirming the authentication details of a particular supposed user, allowing access to private information and programs. A trivial example of such an exploit is illustrated below:

{\small
\begin{lstlisting}[language=C]
 char password[1024];
 scanf("%s", &password);
 bool is_valid_user = strcmp(password, "passwerd1");
 if (is_valid_user) \{
     printf("Access granted!\n");
     // Enter system
 };
\end{lstlisting}
}

    If the user entered more than 1024 bytes of password, then they would have been able to override the is\_valid\_user variable with a value equivalent to \textbf{true}, regardless of whether they actually entered the correct password, \textit{passwerd1}, or not. Although this is an oversimplified example, one could imagine that a more complex overflow could be used to gain access to a real, flawed system in this way.

    This can also be used to gain access to the system on which the code itself is running by using shellcode (see \textbf{section 1.3.3} for details on this). Briefly, this is done by causing a buffer overflow that overrides the return address of a function with no-ops which are indicators for C to continue checking the subsequent addresses - this space past the end of the function's allocated stack space contains our \lq new \rq \ return address. This is calculated to point to where our shellcode that executes a shell is stored. Once the shell code launches the terminal, we would then be able to execute any command with the same privileges as the user which we launched the command from.

    However, it is important to keep in mind that this would only be possible because -fstack-protector is explicitly disabled (and modern compilers enable it by default). Moreover, it was stated that the above was theoretically possible because outside a debugging) gdb environment, Address Space Layout Randomisation (ASLR) would cause the exact calculation of addresses for shellcode execution to be a very complex if not nigh impossible task. ASLR works by randomising the addresses of the base executable, stack, heap, and libraries.

\subsection{iCubeKinect}
\subsubsection{Asymmetric Cipher vs. Symmetric Cipher}
The main reason for the iCubeKinect system to use an asymmetric cipher to verify their DVD games is for authentication. This allows the publishers to authenticate their games by signing with their private key and the consoles to verify the official games by decrypting the signature using their public key. Using this, consoles can all have the same public key to verify signature signed by the matching private key, which only the publishers would have, and is able to verify games.

A symmetric cipher would not allow this, as a shared secret needs to be established between the consoles and the publishers. To do this, one option would be to hard code this shared secret on the machines or DVDs, but this exposes the shared secret, allowing others to take this shared secret and use it to authenticate DVDs on their own. This defeats the purpose as the shared secret is no longer a secret and is expose to everyone. If an approach more similar to Diffie Hellman is taken instead to avoid the shared secret being hard-coded on the machine/DVD, every cryptographic operation on the DVD would require an internet connection with a server somewhere, which would both be irksome for users, and require infrastructure to be able to support, potentially, requests from every single DVD machine of the specified type that exists at any one time on Earth at once.


\subsubsection{iCubeKinect Verification Bug}

The following assumptions on the iCubeKinect are made in identifying the main issues with iCubeKinect's verification. The first assumption is that the private key involved in signing the content hashes of the DVDs is owned by at most a handful of parties, if not just the one, the manufacturer of the consoles themselves. This restricts access to the key, minimising the chance of it being leaked, which would allow anyone to create and sign their own DVDs. It is also assumed that the public keys for verifying those content hashes are hard-coded into the machine, as storing them on each disk would allow anyone to create their own disks using their own private/public key pairs. 

Additionally, we assume that this console is older (e.g. a console in the same generation as the PS2) such that there are no over-the-air (OTA) system updates that would allow content hashes for all the latest games to be periodically pulled from the console maker's remote servers. Rather, we take for granted that the content hashes, metadata, and rsa\_signatures actually sit on each individual DVD itself. We also take it that the content\_hash is in fact some hash (e.g. SHA256, though not too important here) of the \textit{entire} contents of the disk. 

    We also assume that the content hashes are created not by running a hashing algorithm on an entire disk, but only a (globally constant) small, pre-determined subset of it. This is because a check should be performed every single time a disk inserted to ensure that contents are correct, but it would be time consuming to generate hash the entire 4.7GB every single time. This approach makes it difficult for others to keep the subset of data the same when trying to run an arbitrary DVD. Alternative, the entire DVD could have been hashed once, and this hash could be stored locally and saved in the DVD-header as a variable. However, this approach is flawed as that header can be extracted and used as headers of any arbitrary DVD. 

With these assumptions in mind, the main problem in the verification code is on line 14 when the cert\_hash created by the MD5 function contains null terminators. As it is possible for an MD5 hash to include the NULL character, this is an issue when using the \code{strncmp} function. The \code{strncmp} function in C is used to compare the two null terminated strings - but because strings in C are by definition null-terminated char arrays, there is no differentiate between what may be a mid-string null terminator and any null terminator is only taken to indicate the end of a string. The number of characters \textit{actually} checked by strncmp is either MD5\_LENGTH or the number of characters until a null terminator is found, whichever is smaller.

    The serious problem that results is if this null character appear in both the certhash or sighash at the same location (thus allowing a potential match up to that character), the security of the encryption drops significantly. As an example in the general case of doing a pre-image attack on only a prefix of the full MD5 hash; if a null-byte occurred in the 5th character of our hashes, then we would only need to brute force '$2^{7*5} = 34359738368$ hashes in the worst case, only needing to go through 17179869184 on average. Using a Radeon 7970 GPU (which is far from the most powerful consumer GPU available), this would take a mere 2 seconds \footnote{https://blog.codinghorror.com/speed-hashing/}, compared to the computationally intractable brute force pre-image attack on a full MD5 hash. This would allow invalid hash to be identified as valid, allowing DVDs that should not be able to be played, to be played.

To apply this flaw and make the machine execute any arbitrary DVD, we simplify the above scenario to brute force both the signature and certificate hash such that the first byte of both are null bytes and write the result to a new DVD, which is trivial in comparison to the full pre-image attack. This gives the desired result because strncmp will then terminate the check at the very first (null) character, seeing that both match up to this point, allowing the verify\_certificate function to return RSA\_CERT\_OK. Two separate operations need to occur to carry this out:
\begin{enumerate}
        \item The resulting MD5 hash of the concatenated content\_hash and metadata need to start with the null character. To achieve this, while meeting the console's actual content\_hash confirmation, we can change the metadata until this hash results in a leading null byte (assuming the metadata does nothing useful)
            \item Our decrypted signature also needs to lead with a null byte, which can be done by setting the original signature to all zeros. This causes the decrypted signature to be all zeros as well, which is equivalent to a null byte. 
\end{enumerate}

With these conditions met, the iCubeKinect verification process will then verify our certificate successfully for any DVD we wish.

\subsubsection{Fixing the Signing Bug}

The most straightforward fix is to not use C's built-in strncmp function (or any in-built string-related function, which relies on null terminators), but to simply iterate over both strings by exactly MD5\_LENGTH characters, comparing and ensuring equality of every char along the way, null characters included - this will prevent premature termination of the comparison loop. Because the occurrence of the NULL characters is independent of the hashing algorithm, SHA-512 would not necessarily help, as an attacker can still render it useless by simply making the first character null. The other is to use a more suitable implementation so that NULLs do not appear in the hashes at all, making it a non-issue.

\subsection{General Questions}

\subsubsection{GCC's -fno-stack-protector Flag, Canaries and Buffer Overflows}
The -fno-stack-protector is required because as of gcc version 4.8.3, -fstack-protection is on by default\footnote{https://www.gentoo.org/support/news-items/2014-06-15-gcc48\_ssp.html}. To quote directly from the gcc's man pages, the -fstack-protector does the following: \textquotedblleft Emit extra code to check for buffer overflows, such as stack smashing attacks.  This is done by adding a guard variable to functions with vulnerable objects.  This includes functions that call \lq alloca\rq, and functions with buffers larger than 8 bytes.  The guards are initialized when a function is entered and then checked when the function exits.  If a guard check fails, an error message is printed and the program exits.\textquotedblright \ This means that any overflows we wish to exploit that go more than 8 bytes beyond what was originally allocated, the program will state that \lq stack smashing \rq \ was detected, and subsequently exit. A trivial example of this would be to enter the following as the name for savegame.c: \code{justinting..123456789}, whereas \code{justinting..12345678} does not cause a stack smashing error and subsequent exit.

This default protects against buffer overflow attacks such as stack smashing, which is essentially the introduction of more information into a particular segment of stack than space is available. This can be used by hackers to corrupt pointers (the return address of a function, to be specific) to point to places in memory that were not originally intended, where malicious code may reside. As the now-default stack protector prevents this, the -fno-stack-protection flag needs to be explicitly enabled to allow such behaviour for illustrative purposes. Without it, the program would instead throw a segmentation fault at the first sign of any modification of the stack for buffers larger than 8 bytes, as specified in gcc's man pages, possibly preventing any exploitative operations that want to be tested, such as shellcode execution via address redirection.

    A canary in the context buffer overflow is an extra buffer introduced at compile time (that is then used during run-time) that sits between all the stack variables/etc. and the return addresses, such that if an attacker attempted to use a buffer overflow to overwrite the return address for malicious purposes, the canary would similarly be modified, exposing the buffer overflow (deliberate or otherwise). While this incurs extra instructions for every function call before every return, it also protects against redirecting functions to other areas in memory that may contain malicious code. 

    \subsubsection{Buffer Overflows - C vs. Java}
The savegame would no longer be exploitable through means of buffer overflows. This is because array out of bounds errors are always automatically checked in Java, and since Strings use char arrays as their underlying data structure, overflows will not pollute any variables following it. As such, the problems previously described would not arise as a result of the Java language itself. Of course, code is not perfect, and particular implementations of the JVM may contain bugs that would allow buffer overflow exploits such as those in C, as Java is written in C itself.

\subsubsection{Executing Shellcode}

With escalated privileges, it is possible to obtain a BASH shell using buffer overflows. To do this, we need to cause an overflow in the function such that the return address is overwritten with the address of where the shellcode is stored, meaning that once the function has been executed, it would jump to the address of the source of the overflow, where the shellcode is contained. Shellcode is code which can be injected into another program as payload by exploiting vulnerabilities in the code like buffer overflows. 

There are three pieces of information needed to start up a shell using a buffer overflow: 
\begin{enumerate}
    \item Shellcode which contains code that executes a system call to starts up a bash shell
    \item The amount of padding needed
    \item The memory address of where the input array is stored
\end{enumerate}
The input string would have a similar structure to \code{NOP*padding size + Shell Code + memory address of the allocated input array}. With this, when the code reaches to the return address, instead of exiting the program, it would get redirected to the memory address of the input array which contains NOP operations and the shellcode. The NOP operation causes the stack pointer to skip to the next instruction, and this continues until it hits the shellcode and runs it, starting up a BASH shell. 

To calculate the amount of padding needed, the amount needed to create an overflow that overwrites the return address is calculated. Once we have that amount, we can subtract the size of the shellcode and the return address to obtain the size of the padding. This padding is filled using NOP (\textbackslash0x90) which is added at the beginning of the shellcode so that when the stack pointer lands onto the input array, it skips along until it reaches the shellcode. By creating the NOP padding, as long as the memory address that overwrites the original return address landed on one of the NOP operations, it would advance until it hits the code. 

However, due to Address Space Layout Randomisation (see \textbf{section 1.1.3}), the calculations mentioned above are no longer trivial, making it more of a theoretical task when dealing with modern, well written programs.

\section{SQL Questions}
\subsection{SQL Injection Login}

To login as any user, enter their username in the username field, and by escaping the quotes and using an OR statement, you can make the whole query evaluate as true. An example of this would be putting \code{\textquotesingle OR 1=1 --} in the password. The \lq \code{--}\rq \ comments out the rest of the query, the quote closes the current quote and using \lq OR true\rq \ would evaluate the whole statement as true, allowing a hacker to login as anyone in the system. 

Also, if the website is comparing the exact password, it means that they are storing the password in plaintext in the database, which is very insecure, as a hack would result in all the plaintext passwords being instantly compromised. Passwords should always be stored in hashed form using a salt.

\subsection{Extraction of Administrator Password}

To get the password of Bobby, we need to understand the current schema of the database. Invalid syntax can be inputted as user input to cause the program to throw an error, giving information on the table name. This can be seen in the figure below: 
\includegraphics[scale=0.3]{getTableName}

After getting the table name, we can obtain the column name of the table as shown in the following figure by making use of union. 
\includegraphics[scale=0.24]{getColumnName}

Similarly, to get the actual password, we can give an invalid username and use union to get the password of Bobby using the following SQL- \code{\textquotesingle UNION SELECT password FROM Users Where username = \textquotesingle Bobby}, where the rest of the query is commented out by adding \code{--}.  Note that Union will combine the result of the first SQL with the second SQL query, and to get the password, we want to make sure that the first query fails. To ensure that, we can actually use a AND operation, such that the SQL would become \code{\textquotesingle AND 1 = 0 UNION SELECT password FROM Users Where username = \textquotesingle Bobby}

\subsection{Preventing SQL Injections}

By escaping the characters, using prepared statements, stored procedures and input validation,these attacks can be prevented. In injection.py, instead of using \code{cur.execute("SELECT status FROM Users WHERE username =\textquotesingle \%s \textquotesingle" \% username)} which directly substitutes a user’s input into the query, a better way to do this would be to run \code{cur.execute("SELECT status FROM Users WHERE username =?" , [username])} which escapes the characters such as quotes into \textquotesingle. Another good practice would be to catch all errors that is thrown, as these errors provides information about the code, which we want to prevent.  

SQL injection is not a difficult security problem to fix technically speaking, but one small loophole can be disastrous. This is one of the main reasons why SQL injection is so common - if there is even one SQL query which is not secured, it becomes a vulnerability for hackers to exploit. Another reason might be because some companies outsource their web development overseas and build their websites from scratch instead of using a framework. Moreover, most companies would focus on developing new features and not securing an existing codebase, as on its surface, adding that extra security does not seem to have a return on investment. On the other hand, SQL injections are also commonly found in legacy code, which might be because developers rarely go back and patch security flaws on legacy code.

\end{multicols}
\end{document}
